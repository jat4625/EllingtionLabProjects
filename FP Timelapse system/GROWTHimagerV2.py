import os
import datetime
import time
import RPi.GPIO as GPIO
import threading
from picamera import PiCamera
from fractions import Fraction
#import tkinter

#DATE = datetime.datetime.now()
def time_update():
    DayCur = datetime.datetime.now()
    time = DayCur.strftime("%Y-%m-%d")
    return time

def image_control(vidLength, FPS, file_path, complete_file, increment):
    camera = PiCamera()
    camera.shutter_speed = 250000
    camera.ISO = 800
    count = 0

    while count < (int(vidLength) * int(FPS)):
        count += 1
        count_P = str(count)
        file = open(complete_file, 'a')
        GPIO.output(14, 0)  # indicator on
        time.sleep(30)
        image_file = os.path.join(file_path + str(DATEcur), 'image_' + str(time_update()) + '.txt')
        image = open(image_file, 'w')
        image.write('Memes')
        image.close()
        time.sleep(2)
        GrowState = GPIO.input(4)  # current growth light state
        file.write(str(datetime.datetime.now()) + '\n')

        file.write('Growth light off' + '\n')
        GPIO.output(4, 1)
        time.sleep(1)
        file.write('UV light on' + '\n') #adjust to see how plants glow
        GPIO.output(18, 0)
        time.sleep(20)

        # taking the still
        GPIO.output(18, 1)
        time.sleep(5)
        imagepath = os.path.join(file_path + str(DATEcur), 'Frame-' + count_P + '.jpg')
        file.write('Imaging...' + count_P + '\n')
        camera.annotate_text = str(time_update())  # add annotations
        camera.capture(imagepath)
        #UNCOMMENT TO MAKE CAMERA WORK
        # turn off UVlight
        file.write('UV light off' + '\n')
        GPIO.output(18, 1)  # UV off
        GPIO.output(14, 1)  # indicator off
        file.write('Growth light return' + '\n')
        GPIO.output(4, GrowState)  # returns growth light to previous state
        file.close()
        if os.path.exists(image_file):
            os.remove(image_file)
        time.sleep(increment)
    path = 'PlantGrowLapse_' + str(DATEcur)
    file = open(complete_file, 'a')
    file.write('End Time:' + str(datetime.datetime.now()))
    readme = os.path.join(file_path + str(DATEcur), 'readme.txt')
    readmeText = open(readme, 'w')
    readmeText.write('Timelapse complete!' + '\n')
    readmeText.write('Frames per second: ' + str(FPS) + '\n')
    #readmeText.write('Export image files to imageMagick on remote device' + '\n')
    #readmeText.write('If remote device not available, compile on the RPi' + '\n')
    readmeText.write('Copy bottom code into terminal:' + '\n')
    readmeText.write('ffmpeg -r ' + str(FPS) + ' -start_number 1  -i /home/pi/Desktop/Ellington/Logs/' + path + '/Frame-%d.jpg /home/pi/Desktop/Ellington/Logs/' + path + '/lapse' + str(time_update()) + '.mp4 -y')
    readmeText.close()
    exit()


def SYS_Status(correction, complete_file, vidlength, file_path, FPS, increment):
    file = open(complete_file, 'a')
    readfile = os.path.join(file_path + str(DATEcur), 'readme.txt')
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(18, GPIO.OUT)  # 18 UVlight
    GPIO.setup(14, GPIO.OUT)  # 14 indicator
    GPIO.setup(22, GPIO.OUT)  # 22 GenOutput (future motor controller)
    GPIO.setup(4, GPIO.OUT)  # 4 Growing light
    # camera controls

    imageFile = os.path.join(file_path + str(DATEcur), 'image_' + str(time_update()) + '.txt')
    p2 = threading.Thread(target=background, args= (complete_file, file_path))
    p3 = threading.Thread(target=image_control, args=(vidlength, FPS, file_path, complete_file, increment))
    #p4 = threading.Thread(target=SYS_Status(correction, DATE, complete_file, vidlength, file_path, FPS, increment))
    p3.start()
    #p4.start()

    # situational controls
    if correction <= 192:
        for i in range(1, int(correction)):  # Light on for 16 hours
            file = open(complete_file, 'a')
            file.write(str(datetime.datetime.now()) + ' SYS '+'\n')
            if os.path.exists(imageFile):  # Checks if image is being taken
                file = open(complete_file, 'a')
                file.write('Image being taken, skip 5 minute check' + '\n')
                file.close() # If image is being taken, skip the cycle
            else:
                file = open(complete_file, 'a')
                GPIO.output(4, 0)
                file.write('Growth on (sys)' + '\n')
                file.close()
            time.sleep(300)

            if os.path.exists(readfile):  # Check every 5 minutes if the script is finished (ReadMe.txt is present)
                GPIO.output(4, 1) # Turns off light before exiting loop
                file = open(complete_file, 'a')
                file.write(str(datetime.datetime.now()) + '\n')
                file.write('Growth cycle end' + '\n')
                file.close()
                exit()
                break
        for i in range(1, 96):  # lights off for 8 hours
            file = open(complete_file, 'a')
            file.write(str(datetime.datetime.now()) + '\n')
            GPIO.output(4, 1)
            file.write('Growth off (sys)' + '\n')
            time.sleep(300)
            if os.path.exists(readfile):
                GPIO.output(4, 1)
                exit()
                break
            file.close()
        file.close()
        p2.start()
    else:
        now = datetime.datetime.now()
        tomorrow = datetime.datetime(now.year, now.month, now.day) + datetime.timedelta(1)
        adj = ((abs(tomorrow - now).seconds / 60) - 1020)/5
        print('night adj: ' + str(adj))
        for i in range(1, int(adj)):  # lights off for 8 hours
            file = open(complete_file, 'a')
            file.write(str(datetime.datetime.now()) + '\n')
            GPIO.output(4, 1)
            file.write('Growth off (sys)' + '\n')
            time.sleep(300)
            if os.path.exists(readfile):
                GPIO.output(4, 1)
                exit()
                break
            file.close()
        file.close()
        p2.start()



def background(complete_file, file_path):
    condition = True
    readfile = os.path.join(file_path + str(DATEcur), 'readme.txt')
    imageFile = os.path.join(file_path + str(DATEcur), 'image_' + str(time_update()) + '.txt')
    while condition:
        file = open(complete_file, 'a')
        for i in range(1, 192):  # This is what needs to be altered
            file = open(complete_file, 'a')
            file.write(str(datetime.datetime.now()) + ' BG '+ '\n')
            file.close()
            if os.path.exists(imageFile):
                file = open(complete_file, 'a')
                file.write('Image being taken, skip 5 minute check' + '\n')
                file.close()
            else:
                GPIO.output(4, 0)
                file = open(complete_file, 'a')
                file.write('Growth on (backgound)' + '\n')
                file.close()
            time.sleep(300)
            if os.path.exists(readfile):  # This also needs to be altered
                GPIO.output(4, 1)
                file = open(complete_file, 'a')
                file.write(str(datetime.datetime.now()) + '\n')
                file.write('Growth cycle end' + '\n')
                file.close()
                exit()
                break
            else:
                pass
        for i in range(1, 96):  # light off for 8 hours
            file = open(complete_file, 'a')
            file.write(str(datetime.datetime.now()))
            GPIO.output(4, 1)
            file.write('Growth off (BG)' + '\n')
            file.close()
            time.sleep(300)
            if os.path.exists(readfile):
                GPIO.output(4, 1)
                exit()
                break
        file.close()



def txt_log(total_R_time, increment, correction, vidLength,
            FPS):  # Creates a new folder and file based on the date of the initialization
    global totaltime
    totaltime = total_R_time
    global incre
    incre = increment
    global corr
    corr = correction
    global VL
    VL = vidLength

    increment = increment
    global DATEcur
    DATEcur = time_update()
    file_path = r'/home/pi/Desktop/Ellington/Logs/PlantGrowLapse_'
    if not os.path.isdir(file_path + str(DATEcur)):
        os.makedirs(file_path + str(DATEcur))
    complete_file = os.path.join(file_path + str(DATEcur), 'log_' + str(DATEcur) + '.txt')

    file = open(complete_file, 'a')
    file.write('Start time and date: ' + str(datetime.datetime.now()))
    file.write('Start Log:  \n')
    file.write(str(total_R_time) + '\n')
    file.write(str(vidLength) + '\n')
    file.write(str(FPS) + '\n')
    file.write(str(increment) + '\n')
    file.write(str(correction) + '\n')

    file.write('Real time in seconds: ' + str(total_R_time) + '\n')
    file.write('Increment in seconds: ' + str(increment) + '\n')
    file.close()

    SYS_Status(correction, complete_file, vidLength, file_path, FPS, increment)


def calc_parameters(totalTime, vidLength, FPS, cycleMin):  # performs conversions between input data about video and experiment parameters
    total_R_time = int(totalTime) * 3600
    increment = (int(total_R_time) / (int(FPS) * int(vidLength)))
    increment = int(increment) - 42
    if increment <= 0:
        increment = (int(total_R_time/3600) / (int(FPS) * int(vidLength)))
    correction = int(cycleMin) / 5
    print('increment' + str(increment))
    txt_log(total_R_time, increment, correction, vidLength, FPS)
    # image_control(int(FPS)* int(vidLength))


def main():  # prompts the use to input integer values and continues to ask until integer requirement is met before asking for the next parameter
    totalTime = input('Enter real time elapsed in hours: ')
    while isinstance(eval(totalTime), int) != True:
        print('invalid input, must be integer')
        totalTime = input('Enter real time elapsed in hours: ')
    pass
    print(totalTime)

    vidLength = input('Enter timelapse video length in seconds: ')
    while isinstance(eval(vidLength), int) != True:
        print('invalid input, must be integer')
        vidLength = input('Enter timelapse video length in seconds: ')
    pass
    print(vidLength)

    FPS = input('Enter timelapse video frames per second: ')
    while isinstance(eval(FPS), int) != True:
        print('invalid input, must be integer')
        FPS = input('Enter timelapse video frames per second: ')
    pass
    print(FPS)
    now = datetime.datetime.now()
    tomorrow = datetime.datetime(now.year, now.month, now.day) + datetime.timedelta(1)
    cycleMin = (abs(tomorrow - now).seconds / 60) - 60
    print(cycleMin)
    calc_parameters(totalTime, vidLength, FPS, cycleMin)


main()



