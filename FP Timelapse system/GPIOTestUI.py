from tkinter import *
import os
import picamera
import datetime
import RPi.GPIO as GPIO
#from PIL import Image, ImageTk
from fractions import Fraction
from time import sleep, time
camerastate = 0
def main():

    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)
    GPIO.setup(4, GPIO.OUT)  # 4 UVlight
    GPIO.setup(18, GPIO.OUT)  # 17 indicator
    GPIO.setup(22, GPIO.OUT)  # 22 Gen output
    GPIO.setup(14, GPIO.OUT)  # 27 growing light
    OG4 = GPIO.input(4)
    OG14 = GPIO.input(14)
    OG22 = GPIO.input(22)
    OG18 = GPIO.input(18)
    top = Tk()
    label = Message(top)
    label.pack()
    
    T = Text(top)
    T.pack()

    frame = Frame(top)
    frame.pack()
    top.title('GPIO Tester')

    def Return():
        out = "Return GPIO settings"
        GPIO.output(4,OG4)
        GPIO.output(18,OG18)
        GPIO.output(22,OG22)
        GPIO.output(14,OG14)
        print("Returned")
        T.delete(1.0, END)
        T.insert(END, out + '\n')
        label.config(text=str(datetime.datetime.now()))

    def GPIO_14_ON():
        out = "GPIO 14 ON"
        GPIO.output(14,1)
        print("GPIO 14 ON")
        T.insert(END, out + '\n')
        label.config(text=out +'\n')

    def GPIO_14_OFF():
        print("GPIO 14 OFF")
        out = "GPIO 14 OFF"
        GPIO.output(14,0)
        T.insert(END, out + '\n')
        label.config(text=out +'\n')

    def GPIO_18_ON():
        out = "GPIO 18 ON"
        #.AppendText("GPIO 18 ON" + '\n')
        print("GPIO 18 ON")
        GPIO.output(18, 1)
        T.insert(END, out + '\n')
        label.config(text=out + '\n')

    def GPIO_18_OFF():
        print("GPIO 18 OFF")
        out = "GPIO 18 OFF"
        GPIO.output(18, 0)
        T.insert(END, out + '\n')
        label.config(text=out + '\n')
        #text.AppendText("GPIO 18 OFF" + '\n')
    def GPIO_4_ON():
        out = "GPIO 4 ON"
        #GPIO.output(14,1)
        print("GPIO 4 ON")
        GPIO.output(4, 1)
        T.insert(END, out + '\n')
        label.config(text=out +'\n')

    def GPIO_4_OFF():
        print("GPIO 4 OFF")
        out = "GPIO 4 OFF"
        GPIO.output(4,0)
        T.insert(END, out + '\n')
        label.config(text=out +'\n')

    def GPIO_22_ON():
        out = "GPIO 22 ON"
        #.AppendText("GPIO 18 ON" + '\n')
        print("GPIO 22 ON")
        GPIO.output(22, 1)
        T.insert(END, out + '\n')
        label.config(text=out + '\n')

    def GPIO_22_OFF():
        print("GPIO 22 OFF")
        out = "GPIO 22 OFF"
        GPIO.output(22, 0)
        T.insert(END, out + '\n')
        label.config(text=out + '\n')
        #text.AppendText("GPIO 18 OFF" + '\n')

    def DATE():
        print("DATE")
        T.insert(END,str(datetime.datetime.now()) + '\n')
        #.AppendText(str(DATE) + '\n')
    def camera():
        camera = picamera.PiCamera(resolution=(1280, 720), framerate=Fraction(1, 6))
        #camera.shutter_speed = 6000000
        #camera.iso = 800
        camera.start_preview()
        sleep(10)
        camera.close()
        
        
    button = Button(frame, text='Date', fg='red', command=DATE)
    button.pack()
    button2 = Button(frame, text='14', fg='red', command=GPIO_14_ON)
    button2.pack()
    button3 = Button(frame, text='14 off', fg='red', command=GPIO_14_OFF)
    button3.pack()
    button4 = Button(frame, text='18', fg='red', command=GPIO_18_ON)
    button4.pack()
    button = Button(frame, text='18 off', fg='red', command=GPIO_18_OFF)
    button.pack()
    button4 = Button(frame, text='4', fg='red', command=GPIO_4_ON)
    button4.pack()
    button = Button(frame, text='4 off', fg='red', command=GPIO_4_OFF)
    button.pack()
    button4 = Button(frame, text='22', fg='red', command=GPIO_22_ON)
    button4.pack()
    button = Button(frame, text='22 off', fg='red', command=GPIO_22_OFF)
    button.pack()
    buttonCamera = Button(frame, text='CAMERA', fg='red', command=camera)
    buttonCamera.pack()
    buttonRESET = Button(frame, text='RESET', fg='red', command=Return)
    buttonRESET.pack()
    top.mainloop()

main()
'''
def main():
    def Return(event):
        out = "Return GPIO settings"
        text.AppendText("Returning to previous state" + '\n')
        print("Returned")
    def GPIO_14_ON(event):
        out = "GPIO 14 ON"
        text.AppendText("GPIO 14 ON" + '\n')
        print("GPIO 14 ON")
    def GPIO_14_OFF(event):
        print("GPIO 14 OFF")
        text.AppendText("GPIO 14 OFF" + '\n')
    def GPIO_18_ON(event):
        out = "GPIO 14 ON"
        text.AppendText("GPIO 18 ON" + '\n')
        print("GPIO 14 ON")
    def GPIO_18_OFF(event):
        print("GPIO 14 OFF")
        text.AppendText("GPIO 18 OFF" + '\n')
    def DATE(event):
        print("DATE")
        DATE = datetime.datetime.now()
        text.AppendText(str(DATE) + '\n')

    #GPIO state readout code to store states

    app = wx.App()
    frame = wx.Frame(None, -1, 'GPIO Tester')
    frame.SetDimensions(400, 100, 400, 400)

    panel = wx.Panel(frame, wx.ID_ANY)
    button14 = wx.Button(panel, wx.ID_ANY, '14 ON', (10, 110))
    button14.Bind(wx.EVT_BUTTON, GPIO_14_ON)
    button14_off = wx.Button(panel, wx.ID_ANY, '14 OFF', (10, 150))
    button14_off.Bind(wx.EVT_BUTTON, GPIO_14_OFF)
    button18 = wx.Button(panel, wx.ID_ANY, '18 ON', (10, 190))
    button18.Bind(wx.EVT_BUTTON, GPIO_18_ON)
    button18_off = wx.Button(panel, wx.ID_ANY, '18 OFF', (10, 230))
    button18_off.Bind(wx.EVT_BUTTON, GPIO_18_OFF)
    buttonDate = wx.Button(panel, wx.ID_ANY, 'DATE', (100, 230))
    buttonDate.Bind(wx.EVT_BUTTON, DATE)
    text = wx.TextCtrl(panel, style=wx.TE_MULTILINE | wx.BORDER_SUNKEN | wx.TE_READONLY |
                                   wx.TE_RICH2, size=(400, 100))
    frame.Show()
    frame.Centre()
    app.MainLoop()
main()
'''