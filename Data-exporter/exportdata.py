import shutil
import os
import os.path as op
import subprocess
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
from email import encoders




def exportByUSB(filename, pathToMountPoint='mp/'):
    if not os.path.exists(filename) or not os.path.exists(pathToMountPoint):
        return 1

    copyComm = ("sudo cp " + filename + " " + pathToMountPoint).split(' ')
    try:
        subprocess.call(copyComm)
    except OSError:
        print('[Err: exportdata.py] File could not be copied!')
    return 0


def exportByMail(useremailID, subjectTxt, messageTxt, files=[]):

    server = 'smtp.gmail.com'
    port = 587
    username = 'ethylenesensor@gmail.com'
    password = 'AKs401GJK'
    msg = MIMEMultipart()
    msg['From'] = username
    msg['To'] = useremailID
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subjectTxt

    msg.attach(MIMEText(messageTxt))

    # send a list of files (Based on the mime docs).
    for path in files:

        fileLoad = MIMEBase('application', "octet-stream")

        with open(path, 'rb') as file:
            fileLoad.set_payload(file.read())

        #use base64 (standard encoding)
        encoders.encode_base64(fileLoad)
        #create the attatchment header
        fileLoad.add_header('Content-Disposition',
                        'attachment; filename="{}"'.format(op.basename(path)))
        msg.attach(fileLoad)

    smtp = smtplib.SMTP(server, port)
    smtp.starttls()
    smtp.login(username, password)
    smtp.sendmail(username, useremailID, msg.as_string())
    smtp.quit()