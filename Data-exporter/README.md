


##exportdata.py

Methods to export a file (given it's valid path) by either email or copied over to a USB drive.

For the email, the login details are: 
ID: ethylenesensor@gmail.com
pass: AKs401GJK
```python3
import exportdata

# export via email
exportdata.exportByMail('userEmail@gmail.com', 'Subject!', 'Body!', ['test.csv'])

# export via usb
exportdata.exportByUSB('test.csv')
```