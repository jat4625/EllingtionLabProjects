from time import time, sleep
import datetime
from Adafruit_MotorHAT_Motors import Adafruit_MotorHAT, Adafruit_DCMotor
# import Motor HAT library to control pump
mh = Adafruit_MotorHAT()
interval = 0  # default = 0 seconds
Init = None
PD = 0  #PD == pump duration in seconds
#log = open(str(datetime.date) +'log.txt' , 'a')
check = 1

def reset():
    global check
    check = 0
    myMotor = mh.getMotor(3)
    myMotor2 = mh.getMotor(4)
    myMotor.run(Adafruit_MotorHAT.RELEASE);
    myMotor2.run(Adafruit_MotorHAT.RELEASE);
def pump():
    print('pumping')
    # activate pump
    myMotor = mh.getMotor(3)
    myMotor2 = mh.getMotor(4)
    for i in range(int(PD)):
        print(i)
        myMotor.setSpeed(150)
        myMotor.run(Adafruit_MotorHAT.FORWARD);
        myMotor2.setSpeed(150)
        myMotor2.run(Adafruit_MotorHAT.FORWARD);
        # turn on motor
        sleep(1)
    myMotor.run(Adafruit_MotorHAT.RELEASE);
    myMotor2.run(Adafruit_MotorHAT.RELEASE);
    print('Finish a cycle')
    pass


def time_check():
    global Init
    print(Init, "Init value")
    now = time()
    delta = now - Init
    if (now - Init) >= interval:
        print(delta, 'Interval met')
        pump()
        Init = time()
        print(Init, 'New Init value')
    else:
        print('interval not met')
        print('Current Interval: ', str(delta))
        return Init
    return Init


def main():
    MinInterval = eval(input("Enter sampling interval in minutes: "))
    pumpDuration = eval(input("Enter pump duration in minutes: "))
    global interval
    interval = MinInterval * 60
    global PD
    PD = pumpDuration * 60
    global Init
    Init = time()
    while True:

        time_check()
        sleep(1)

def WEBmain(Interval,Duration):
    global interval
    interval = Interval * 60
    global PD
    PD = Duration * 60
    global Init
    Init = time()
    global check
    check = 1
    while check == 1:

        time_check()
        sleep(1)
    else:
        pass


if __name__ == '__main__':
    main()






