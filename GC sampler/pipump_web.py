
import remi.gui as gui
from remi import start, App
try:
    import GC_Sampler
except:
    pass

class MyApp(App):
    def __init__(self, *args):
        super(MyApp, self).__init__(*args)

    def main(self):
        #creating a container VBox type, vertical
        wid = gui.VBox(width='100%', height='35%')

        #creating a text label, "white-space":"pre" preserves newline
        self.title = gui.Label('PiPump', width=200, style={"white-space":"pre"})
        self.title.style.update({"font-size":"40px", "text-align":"center"})
        self.lbl = gui.Label('Enter Pump duration in minutes:', width=200, style={"white-space":"pre"})
        self.txt = gui.TextInput(width=200, height=30)
        self.txt.set_text('0')
        self.lbl2 = gui.Label('Enter Pump interval in minutes:', width=200, style={"white-space":"pre"})
        self.txt2 = gui.TextInput(width=200, height=30)
        self.txt2.set_text('0')
        #a button for simple interaction
        bt = gui.Button('Send to Pi', width=200, height=30)
        reset = gui.Button('Reset Pi', width=200, height=30)
        self.lbl3 = gui.Label('', width=200, style={"white-space":"pre"})
        self.lbl3.style.update({"font-size":"20px"})
        #setting up the listener for the click event
        bt.onclick.do(self.on_button_pressed)
        reset.onclick.do(self.on_reset_pressed)

        #adding the widgets to the main container
        wid.append(self.title)
        wid.append(self.lbl)
        wid.append(self.txt)
        wid.append(self.lbl2)
        wid.append(self.txt2)

        wid.append(bt)
        wid.append(reset)
        wid.append(self.lbl3)
        # returning the root widget
        return wid

    # listener function
    def on_button_pressed(self, emitter):
        self.lbl3.set_text('Duration set at: ' + str(self.txt.get_value()) + '\n' + 'Interval set at: ' + str(self.txt2.get_value()))
        GC_Sampler.WEBmain(eval(self.txt.get_value()),eval(self.txt2.get_value()))

    def on_reset_pressed(self, emitter):
        self.lbl3.set_text('Values reset to 0')
        self.txt.set_text('0')
        self.txt2.set_text('0')
        GC_Sampler.reset()
def remi_main(ip, port = 0):
    start(MyApp, debug=True, address=ip, port=port)


if __name__ == "__main__":
    # starts the webserver
    # optional parameters
    # start(MyApp,address='127.0.0.1', port=8081, multiple_instance=False,enable_file_cache=True, update_interval=0.1, start_browser=True)
    start(MyApp, debug=True, address='0.0.0.0', port=0)
