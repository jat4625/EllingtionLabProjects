#import pyautogui
#import pyperclip
import os
import socket
import pipump_web as pw
from time import sleep

def getPort():
    pass
def getIP():
    ip_address = '';
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8",80))
    ip_address = s.getsockname()[0]
    s.close()
    return ip_address

def flyKite():
    port = 8081
    print("Calling PageKite")
    os.system("pagekite.py " + str(port) + " pipump.webwdiget.pagekite.me")
def launchREMI():
    ip = getIP()
    port = 8081
    pw.remi_main(ip, port)
if __name__ == "__main__":
    flyKite()
    launchREMI()
    #print(getIP())
